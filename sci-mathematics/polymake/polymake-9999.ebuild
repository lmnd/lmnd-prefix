# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=2

inherit eutils flag-o-matic subversion

# polymake SVN repository is not publicly available
ESVN_REPO_URI="http://polymake.org/svn/polymake/trunk"
ESVN_OPTIONS="--ignore-externals"

ESVN_RESTRICT="export"
DESCRIPTION="research tool for polyhedral geometry and combinatorics"
HOMEPAGE="http://polymake.org"

IUSE="libpolymake"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"

DEPEND="dev-libs/gmp
	dev-libs/libxml2:2
	dev-perl/XML-LibXML
	dev-libs/libxslt
	dev-perl/XML-LibXSLT
	dev-perl/XML-Writer
	dev-perl/Term-ReadLine-Gnu
	dev-libs/mpfr"
RDEPEND="${DEPEND}"

src_unpack() {
	subversion_src_unpack
	subversion_wc_info
	mkdir -p "${S}"
	# copy including .svn folders
	cd ${ESVN_WC_PATH}
	rsync -rlpgo . "${S}" || die "${ESVN}: can't copy to ${S}."
	cd "${S}"
}

src_prepare() {
	# embedded jreality is a precompiled disaster (bug #346073)
	epatch "${FILESDIR}/${PN}"-2.11-drop-jreality.patch
	# Assign a soname
	epatch "${FILESDIR}/${PN}"-9999-soname.patch

	# Remove jreality and the core extensions
	# extensions are packaged as separate ebuilds
	rm -rf java_build/jreality ext

	# Don't strip
	sed -i '/system "strip $to"/d' support/install.pl || die

	einfo "During compile this package uses up to"
	einfo "750MB of RAM per process. Use MAKEOPTS=\"-j1\" if"
	einfo "you run into trouble."
}

src_configure () {
	export CXXOPT=$(get-flag -O)
	local myconf
	if ! use libpolymake ; then
		myconf="--without-callable"
	fi
	# Configure does not accept --host, therefore econf cannot be used
	./configure --prefix="${EPREFIX}/usr" \
		--without-java \
		--without-prereq \
		--libdir="${EPREFIX}/usr/$(get_libdir)" \
		--libexecdir="${EPREFIX}/usr/$(get_libdir)/polymake" \
		"${myconf}" || die
}

src_install(){
	emake -j1 DESTDIR="${D}" install || die "install failed"
	dosym libpolymake.so "${PREFIX}/usr/$(get_libdir)/libpolymake.so.0" || die
	sed -i -e 's/override DESTDIR/DESTDIR/' \
		   -e 's/INSTALL_PL=/INSTALL_PL:=/' \
			${D}/${EPREFIX}/usr/$(get_libdir)/polymake/conf.make || die
}

pkg_postinst(){
	elog "Polymake uses Perl Modules compiled during install."
	elog "You have to reinstall polymake after an upgrade of Perl."
	elog " "
	elog "This version of polymake does not ship docs. Sorry."
	elog "Help can be found on http://www.opt.tu-darmstadt.de/polymake_doku/ "
	elog " "
	elog "Visualization in polymake is via jreality which ships pre-compiled"
	elog "binary libraries.  Until this situation is resolved, support for"
	elog "jreality has been dropped.  Please contribute to Bug #346073 to "
	elog "make jreality available in Gentoo."
	elog " "
	elog "This is a svn-version and may contain (lots of) bugs!"
}
