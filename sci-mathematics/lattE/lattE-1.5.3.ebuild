# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

DESCRIPTION="lattE-integrale consists of tools for lattice point enumeration"
# upstream only ships a tarball with all dependencies included at
#    http://www.math.ucdavis.edu/~latte/software/latte-integrale-1.5.3.tar.gz
# lmonade mirrors the real latte-integrale sources from within that tarball
SRC_URI="http://www.lmona.de/files/distfiles/latte-int-1.5.tar.gz"

HOMEPAGE="http://www.math.ucdavis.edu/~latte/software.php"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64 ~amd64-linux ~x86-linux ~ppc-macos ~x86-macos ~x64-macos"
IUSE=""

DEPEND="dev-libs/gmp[cxx]
	>=dev-libs/ntl-5.4.2
	sci-mathematics/4ti2
	>=sci-libs/cdd+-077a
	>=sci-mathematics/glpk-4.13
	>=sci-libs/cddlib-094f"
RDEPEND="${DEPEND}"

S="${WORKDIR}/latte-int-1.5"

src_configure() {
	econf --with-ntl="${EPREFIX}"
}
