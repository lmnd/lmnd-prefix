# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

# Michał Górny <mgorny@gentoo.org> (26 Jan 2013)
# Unmask the x32 ABI.
-abi_x86_x32
