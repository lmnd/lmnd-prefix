# Copyright 1999-2013 Gentoo Foundation.
# Distributed under the terms of the GNU General Public License v2
# $Header$

nls
pam

emul-linux-x86
-uclibc

-elibc_uclibc
elibc_glibc

-hardened
