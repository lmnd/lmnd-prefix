# Copyright 1999-2013 Gentoo Foundation.
# Distributed under the terms of the GNU General Public License v2
# $Header$

#
# Since mips is a ~arch, we need to mask some extra packages here
#
>dev-lang/perl-5.12.4-r2
>dev-libs/libpcre-8.30-r2
#
# Broken packages
#
app-cdr/cdrtools
