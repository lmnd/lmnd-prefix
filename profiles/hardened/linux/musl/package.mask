# Copyright 1999-2013 Gentoo Foundation.
# Distributed under the terms of the GNU General Public License v2
# $Header$

-sys-libs/musl
sys-libs/uclibc
sys-libs/glibc
#
dev-libs/elfutils
sys-libs/pam
